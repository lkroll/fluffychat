import QtQuick 2.0
import Lomiri.Components 1.3
import Lomiri.Connectivity 1.0

Connectivity {
    onOnlineChanged: matrix.online = online
}
